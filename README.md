# How to run the exercises

* Download the official version of Palabos
* Create the directory `examples/summerSchool` inside this copy of Palabos
* Move the example into this directory. For example: `mv MondayMorning/dsl2d PALABOS_DIRECTORY/examples/summerSchool/`
