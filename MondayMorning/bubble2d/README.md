# Data processors, composite dynamics and modern collision models: Application to the simulation of a 2D bubble

## Introduction
The goal of this exercise is to gain familiarity with the concept of data processors, as well as, composite dynamics. To do so, a 2D bubble will be simulated through Shan-Chen interparticle force for single component, multiphase flows [1]. This external force is accounted for through Guo's forcing approach [2], and it can be coupled with a number of collision models already present in Palabos via the concept of composite dynamics. 

Your mission, should you choose to accept it, will be to (1) implement parts of the data processor related to this external force by filling in the blanks in the code `utility_bubble2d.h`, and to (2) play with three collision models to quantify their impact on spurious currents. 

#### Task list
1. Test the compilation with CMake
2. Correct the code by replacing four displaced lines into their proper location.
3. Try different collision models;


## Guide
### Exercise 1: Test the compilation
Open a terminal in the current directory and type the following commands to compile the code with cmake:
```
    cd build
    cmake .. && make -j
    cd ..
```
At this point you should have an executable in the current directory named `bubble2d`. Once the code has been modified according to instructions detailed in the following sections, you should be able to run it in parallel (in the following example with four MPI tasks) via
```
    mpirun -np 4 ./bubble2d
```

### Exercise 2: fill in the missing lines
Open the file `utility_bubble2d.h` and search for the four commented extra lines in the beginning of the file (search for "EXTRA LINE"). Put all four lines into their right place (attention: you need to restore the proper order between the lines) by searching for "MISSING LINE" in the file. The four commented lines to be filled into the right place are:
```
        // EXTRA LINE 1:
        //force.from_cArray(cell.getExternal(Descriptor<T>::ExternalField::forceBeginsAt));
        //
        // EXTRA LINE 2:
        //*(cell.getExternal(forceOffset)+iD) = - G * psi * rhoContribution[iD];        // Force formulation
        //
        // EXTRA LINE 3:
        //rhoContribution[iD] += D::t[iPop] * psi * D::c[iPop][iD];
        //
        // EXTRA LINE 4:
        //externalForceTemplates<T,Descriptor>::addGuoForce(cell, j, this->getOmega(), forceIsAnAcceleration? rho: (T)1.);
```
Below, some explanations are provided about the data processor and the Dynamics object into which to fill these four lines.

#### Explanation: Data processor for Shan-Chen interparticle force without in-place modification of the momentum
In the file `utility_bubble2d.h`, you will find a data processor named `ShanChenSingleComponentNoMomCorrProcessor2D`. The latter is a modified version of `ShanChenSingleComponentProcessor2D`, in the sense that it does not include the momemtum correction typical of Shan-Chen's approach [1]. Instead, the momentum correction in done through Guo's methodology (see next section and Ref[2] for more details).

Eventually, it is worth noting that the `force formulation` was adopted for this example. Nevertheless, one can easily move from force to acceleration formulation by simply (un)commenting corresponding lines in 
```
            // Computation and storage of the force due to interaction potential
            Cell<T,Descriptor>& cell = lattice.get(iX,iY);
            //T rho = cell.computeDensity(); // Acceleration formulation
            for (int iD = 0; iD < D::d; ++iD) {
                T psi = psiField.get(iX-offsetX, iY-offsetY);
                // MISSING LINE:
                // This line is inside the data processor. Here, we write the
                // computed interparticle force into an external scalar.
            }
```
and modifying the boolean value `forceIsAnAcceleration` accordingly in `bubble2d.cpp`.

#### Explanation: Combining Guo's forcing approach with different collision models
In the file `utility_bubble2d.h`, you will find a class named `GuoCompositeDynamics`. The latter is a composite class that aims at combining any collision model with Guo's forcing methodology. Looking at collide() 
```
    template<typename T, template<typename U> class Descriptor>
    void GuoCompositeDynamics<T,Descriptor>::collide (
            Cell<T,Descriptor>& cell, BlockStatistics& statistics )
    {
        Array<T,Descriptor<T>::d> force;
        // MISSING LINE:
        // This line is in the collision term of the dynamics model.
        // We access the external scalar to get the force term previously computed by the data processor.

        Array<T,Descriptor<T>::d> j;
        T rhoBar;
        baseDynamics->computeRhoBarJ(cell, rhoBar, j);
        T rho = Descriptor<T>::fullRho(rhoBar);
        // If the force is an acceleration, e.g., F = g then u[iD] += 0.5 * F[iD] -->  j[iD] += 0.5 * F[iD] * rho
        // otherwise, if it is a force, then  F = rho * g and u[iD] += 0.5 * F[iD] / rho -->  j[iD] += 0.5 * F[iD]
        for (plint iD = 0; iD < Descriptor<T>::d; ++iD)
        {
            j[iD] += 0.5 * force[iD] * (forceIsAnAcceleration? rho: (T)1.);
        }
        T thetaBar = 0.;
        // Guo's forcing term is coded in terms of force, hence one must multiply it by rho
        // in case an acceleration is used (F=g). The amplitude parameter is used to do it.
        baseDynamics -> collideExternal(cell, rhoBar, j, thetaBar, statistics);
        // MISSING LINE
        // This line is in the collision term of the dynamics model.
        // We implement the Guo forcing term to apply the force.
    }
```
post-collision populations are computed through the function `collideExternal()` from the base dynamics 
```
    baseDynamics -> collideExternal(cell, rhoBar, j, thetaBar, statistics);
```
`collideExternal()` is used instead of `collide()` because the momemtum j is modified to account for the external forces/acceleration
```
    for (plint iD = 0; iD < Descriptor<T>::d; ++iD)
    {
        j[iD] += 0.5 * force[iD] * (forceIsAnAcceleration? rho: (T)1.);
    }
```
where the boolean `forceIsAnAcceleration` allows for the distinction between an external force and an external acceleration (make sure that it has the correct value in `bubble2d.cpp` when running the code).
After that, Guo's forcing term is added to post-collision populations via addGuoForce().

This composite dynamics is called in `bubble2d.cpp` via
```
    MultiBlockLattice2D<T, DESCRIPTOR> lattice (
            nx,ny,new GuoCompositeDynamics<T, DESCRIPTOR>(dyn, forceIsAnAcceleration) );
```
where, in the present example, `dyn` is the dynamics related to the collision model of interest. Even if this composite dynamics can theoretically work with all collision models available in Palabos, it cannot be used, at the moment, with those implemented in `comprehensiveModelsTemplates` [3,4]. Nevertheless, such a feature will be available in a future release of Palabos. 

#### Impact of the collision model on spurious currents
To modify the collision model, one just need to choose `dynName` in `bubble2d.cpp` among (1) `BGK_Ma2`, (2) `Complete_BGK` and (3) `Complete_Regularized_BGK`. The first dynamics corresponds to the BGK collision model with a second-order equilibrium, whereas the second one is based on the complete equilibrium, i.e., that also includes some high-order velocity terms. The last collision model is based on a recursive regularization, proposed by Orestis Malaspinas [5], and which boils down to one of the filtered collision term used in PowerFLOW software [6].

In the context of single phase, single component flow simulations, the collision model (3) should be preferred when the viscosity is small. It is also interesting to see that it further helps reducing the amplitude of spurious currents by a factor 2-3, when simulating multiphase flows, and this, without increasing the bulk viscosity. This is explained by the fact that more robust collision models introduce more numerical dissipation (at the grid cutoff size), hence, the reduced amplitude of spurious currents. 

## Bibliography
[1] Shan, X., & Chen, H. (1993). Lattice Boltzmann model for simulating flows with multiple phases and components. Physical Review E, 47(3), 1815.

[2] Guo, Z., Zheng, C., & Shi, B. (2002). Discrete lattice effects on the forcing term in the lattice Boltzmann method. Physical Review E, 65(4), 046308.

[3] Coreixas, C., Chopard, B., & Latt, J. (2019). Comprehensive comparison of collision models in the lattice Boltzmann framework: Theoretical investigations. Physical Review E, 100(3), 033305.

[4] Coreixas, C., Wissocq, G., Chopard, B., & Latt, J. (2020). Impact of collision models on the physical properties and the stability of lattice Boltzmann methods. Philosophical Transactions of the Royal Society A, 378(2175), 20190397.

[5] Malaspinas, O. (2015). Increasing stability and accuracy of the lattice Boltzmann scheme: recursivity and regularization. arXiv preprint arXiv:1505.06900.

[6] Chen, H., Zhang, R., & Gopalakrishnan, P. (2020). Filtered lattice Boltzmann collision formulation enforcing isotropy and galilean invariance. Physica Scripta, 95(3), 034003.
