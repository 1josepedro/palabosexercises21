/* This file is part of the Palabos library.
 *
 * The Palabos softare is developed since 2011 by FlowKit-Numeca Group Sarl
 * (Switzerland) and the University of Geneva (Switzerland), which jointly
 * own the IP rights for most of the code base. Since October 2019, the
 * Palabos project is maintained by the University of Geneva and accepts
 * source code contributions from the community.
 * 
 * Contact:
 * Jonas Latt
 * Computer Science Department
 * University of Geneva
 * 7 Route de Drize
 * 1227 Carouge, Switzerland
 * jonas.latt@unige.ch
 *
 * The most recent release of Palabos can be downloaded at 
 * <https://palabos.unige.ch/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* This file was written by Christophe Coreixas and Jonas Latt for the
 * Palabos Summer School 2021.
 */

#include "palabos2D.h"
#include "palabos2D.hh"
#include "utility_bubble2d.h"
#include <cstdlib>
#include <iostream>

using namespace plb;
using namespace std;

typedef double T;


#define DESCRIPTOR descriptors::ForcedD2Q9Descriptor

/// Initialization of a bubble/droplet.
template<typename T, template<typename U> class Descriptor>
class Bubble2D : public BoxProcessingFunctional2D_L<T,Descriptor> 
{
public :
    Bubble2D(T nx_, T ny_, T rho_g_, T rho_l_) : nx(nx_), ny(ny_), rho_g(rho_g_), rho_l(rho_l_)
    { };
    virtual void process(Box2D domain,BlockLattice2D<T,Descriptor>& lattice)
    {
        for (plint iX = domain.x0; iX <= domain.x1; ++iX) {
            for (plint iY = domain.y0; iY <= domain.y1; ++iY)
            {
                // Initialize with the gas density
                T rho = rho_g;
                
                // Flow at rest
                Array<T,2> zeroVelocity (0.,0.);
                
                // No external force
                Array<T,2> force (0.,0.);

                // Put a droplet of radius R at the center of the simulation domain
                Dot2D offset = lattice.getLocation();
                T x  = (T)(iX+offset.x);
                T y  = (T)(iY+offset.y);
                T xC = (T)(x-(nx-1.)/2.)/(T)(nx);
                T yC = (T)(y-(ny-1.)/2.)/(T)(ny);
                T R = 1./5.;
                if (xC*xC+yC*yC < R*R) rho = rho_l;

                // Initialize populations at equilibrium via rho and u
                iniCellAtEquilibrium(lattice.get(iX,iY), rho, zeroVelocity);
                
                // Initialize the force field for Shan-Chen coupling
                lattice.get(iX,iY).setExternalField (
                        Descriptor<T>::ExternalField::forceBeginsAt,
                        Descriptor<T>::ExternalField::sizeOfForce, &force[0] );
            }
        }
    };
    virtual Bubble2D<T,Descriptor>* clone() const
    {
        return new Bubble2D<T,Descriptor>(*this);
    };
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const
    {
        modified[0] = modif::staticVariables;
    };
    
private :
    T nx, ny, rho_g, rho_l;
};

/// Write the velocity-vector, the velocity-norm and the density into a VTK file.
/// Everything is in lattice units
void writeVTK(MultiBlockLattice2D<T,DESCRIPTOR>& lattice, plint iter)
{
    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iter, 6), 1);
    vtkOut.writeData<float>(*computeDensity(lattice), "density", 1.0);
    vtkOut.writeData<float>(*computeVelocityNorm(lattice), "velocityNorm", 1);
}

int main(int argc, char *argv[])
{
    plbInit(&argc, &argv);
    global::directories().setOutputDir("./tmp/");
    
    const T omega = 1./(0.1+0.5);  // Relaxation frequency for the kinematic viscosity

    const int nx   = 200;          // Domain size (LB units)
    const int ny   = 200;          // Domain size (LB units)

    const int maxIter  = 30001;
    const int saveIter = 1000;
    const int statIter = 100;
    
    // Dynamics used for the simulation
    string dynName = "BGK_Ma2";
    
    ///// Generate the dynamics.
    Dynamics<T,DESCRIPTOR> *dyn;
    if (dynName == "BGK_Ma2") {                         // BGK with second-order equilibrium
        dyn = new BGKdynamics<T,DESCRIPTOR>(omega);
    } else if (dynName == "Complete_BGK") {             // BGK with partial fourth-order equilibrium
        dyn = new CompleteBGKdynamics<T,DESCRIPTOR>(omega);
    } else if (dynName == "Complete_Regularized_BGK") { // Complete regularization aka Recursive regularization with partial fourth-order equilibrium
        dyn = new CompleteRegularizedBGKdynamics<T,DESCRIPTOR>(omega);
    } else {
        pcout << "Error: dynamics name does not exist." << std::endl;
        exit(-1);
    }
    
    //// Include Guo's forcing methodology into a particular collision dynamics.
    bool forceIsAnAcceleration = false;
    MultiBlockLattice2D<T, DESCRIPTOR> lattice (
            nx,ny,new GuoCompositeDynamics<T, DESCRIPTOR>(dyn, forceIsAnAcceleration) );

    //// Periodic boundary conditions
    lattice.periodicity().toggleAll(true);

    //// Equilibrium densities 
    T rho_g = 0.15;
    T rho_l = 1.95;    
    
    //// Initialize a droplet at the center of the simulation domain.
    applyProcessingFunctional(new Bubble2D<T,DESCRIPTOR>(nx, ny, rho_g, rho_l), 
                              lattice.getBoundingBox(),lattice);

    //// Here Shan/Chen 1993 with psi = rho0 * (1-exp(-rho/rho0))
    // Model constants
    const T rho0 = 1.; // So that rho0 disappears from formulas
    // Add the data processor which implements the Shan/Chen interaction potential.
    const T G    = -5; // G = G'/cs2 with 1/cs2=3 
                       // since cs2 is already included in the computation of grad(psi)
    plint processorLevel = 1;
    integrateProcessingFunctional (
            new ShanChenSingleComponentNoMomCorrProcessor2D<T,DESCRIPTOR> (
                G, new interparticlePotential::PsiShanChen93<T>(rho0) ),
            lattice.getBoundingBox(), lattice, processorLevel );

    lattice.initialize();
    
    //// Main loop
    pcout << "Starting simulation" << endl;
    for (int iT=0; iT<maxIter; ++iT) {
        if (iT%statIter==0) {
            std::unique_ptr<MultiScalarField2D<T> > rho( computeDensity(lattice) );
            pcout << iT << ": Average rho fluid one = " << computeAverage(*rho) << endl;
            pcout << "Minimum density: " << computeMin(*rho) << endl;
            pcout << "Maximum density: " << computeMax(*rho) << endl;
        }
        if (iT%saveIter == 0) {
            ImageWriter<T>("leeloo").writeScaledGif (
                    createFileName("rho", iT, 6), *computeDensity(lattice) );
            pcout << "Saving VTK file ..." << endl;
            writeVTK(lattice, iT);
        }
        lattice.collideAndStream();
    }
}
