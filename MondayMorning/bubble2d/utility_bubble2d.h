/* This file is part of the Palabos library.
 *
 * The Palabos softare is developed since 2011 by FlowKit-Numeca Group Sarl
 * (Switzerland) and the University of Geneva (Switzerland), which jointly
 * own the IP rights for most of the code base. Since October 2019, the
 * Palabos project is maintained by the University of Geneva and accepts
 * source code contributions from the community.
 * 
 * Contact:
 * Jonas Latt
 * Computer Science Department
 * University of Geneva
 * 7 Route de Drize
 * 1227 Carouge, Switzerland
 * jonas.latt@unige.ch
 *
 * The most recent release of Palabos can be downloaded at 
 * <https://palabos.unige.ch/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* This file was adapted from two data processors written by Andrea Parmigianni 
 * and Orestis Malasipnas, by Christophe Coreixas and Jonas Latt.
 */

#ifndef UTILITY_BUBBLE2D_H
#define UTILITY_BUBBLE2D_H

#include "multiPhysics/shanChenProcessor2D.h"
#include "core/util.h"
#include "finiteDifference/finiteDifference2D.h"
#include "latticeBoltzmann/momentTemplates.h"
#include "latticeBoltzmann/externalFieldAccess.h"
#include "multiPhysics/multiPhaseTemplates2D.h"

#include "basicDynamics/isoThermalDynamics.h"
#include "core/cell.h"
#include "latticeBoltzmann/dynamicsTemplates.h"
#include "latticeBoltzmann/geometricOperationTemplates.h"
#include "latticeBoltzmann/externalForceTemplates.h"
#include "core/latticeStatistics.h"
#include <algorithm>
#include "core/dynamicsIdentifiers.h"
#include <limits>

// The following three lines got removed from the code. Search for
// "MISSING LINE" and put them back!
//
// EXTRA LINE 1:
//force.from_cArray(cell.getExternal(Descriptor<T>::ExternalField::forceBeginsAt));
//
// EXTRA LINE 2:
//*(cell.getExternal(forceOffset)+iD) = - G * psi * rhoContribution[iD];        // Force formulation
//
// EXTRA LINE 3:
//rhoContribution[iD] += D::t[iPop] * psi * D::c[iPop][iD];
//
// EXTRA LINE 4:
//externalForceTemplates<T,Descriptor>::addGuoForce(cell, j, this->getOmega(), forceIsAnAcceleration? rho: (T)1.);

namespace plb {

/* *************** ShanChenSingleComponentNoMomCorrProcessor2D ***************** */
// Identical to ShanChenSingleComponentProcessor2D (shanChenProcessor2D.hh) but without 
// momentum correction so that Shan-Chen model can be used with other forcing methods
// (e.g., with Guo's forcing methodology)
    
template<typename T, template<typename U> class Descriptor>
class ShanChenSingleComponentNoMomCorrProcessor2D : public BoxProcessingFunctional2D_L<T,Descriptor> {
public:
    ShanChenSingleComponentNoMomCorrProcessor2D(T G_, interparticlePotential::PsiFunction<T>* Psi_);
    virtual ~ShanChenSingleComponentNoMomCorrProcessor2D();
    ShanChenSingleComponentNoMomCorrProcessor2D(ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor> const& rhs);
    ShanChenSingleComponentNoMomCorrProcessor2D& operator=(ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor> const& rhs);
    virtual void process(Box2D domain, BlockLattice2D<T,Descriptor>& lattice );
    virtual ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
private:
    T G;
    interparticlePotential::PsiFunction<T>* Psi;
};



template<typename T, template<typename U> class Descriptor>
ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor>::ShanChenSingleComponentNoMomCorrProcessor2D (
        T G_, interparticlePotential::PsiFunction<T>* Psi_ )
    : G(G_),
      Psi(Psi_)
{ }

template<typename T, template<typename U> class Descriptor>
ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor>::~ShanChenSingleComponentNoMomCorrProcessor2D() {
    // Pointer to Psi function is owned; delete it in the destructor.
    delete Psi;
}

template<typename T, template<typename U> class Descriptor>
ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor>::ShanChenSingleComponentNoMomCorrProcessor2D (
        ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor> const& rhs )
    : G(rhs.G),
      Psi(rhs.Psi->clone())
{ }

template<typename T, template<typename U> class Descriptor>
ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor>&
    ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor>::operator= (
        ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor> const& rhs )
{
    G = rhs.G;
    delete Psi; Psi = rhs.Psi->clone();
    return *this;
}

template<typename T, template<typename U> class Descriptor>
ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor>*
    ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor>::clone() const
{
    return new ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor>(*this);
}

template<typename T, template<typename U> class Descriptor>
void ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor>::getTypeOfModification(std::vector<modif::ModifT>& modified) const
{
    modified[0] = modif::staticVariables;
}

template<typename T, template<typename U> class Descriptor>
void ShanChenSingleComponentNoMomCorrProcessor2D<T,Descriptor>::process (
        Box2D domain, BlockLattice2D<T,Descriptor>& lattice )
{
    // Short-hand notation for the lattice descriptor
    typedef Descriptor<T> D;
    // Handle to external scalars
    enum {
        forceOffset    = D::ExternalField::forceBeginsAt
    };

    plint nx = domain.getNx() + 2;  // Include a one-cell boundary
    plint ny = domain.getNy() + 2;  // Include a one-cell boundary
    plint offsetX = domain.x0-1;
    plint offsetY = domain.y0-1;
    ScalarField2D<T> psiField(nx,ny);
    
    // Evaluate the interaction potential Psi and store it into a ScalarField.
    //   Envelope cells are included, because they are needed to compute the interaction potential
    //   in the following.
    for (plint iX=domain.x0-1; iX<=domain.x1+1; ++iX) {
        for (plint iY=domain.y0-1; iY<=domain.y1+1; ++iY) {
            // Get "intelligent" value of density through cell object, to account
            //   for the fact that the density value can be user-defined, for example
            //   on boundaries.
            Cell<T,Descriptor>& cell = lattice.get(iX,iY);
            T rho = cell.computeDensity();
            // Evaluate potential function psi.
            psiField.get(iX-offsetX, iY-offsetY) = Psi->compute(rho);
        }
    }

    // Compute the interparticle forces, and store them in the external force field.
    for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
            Array<T,D::d> rhoContribution;
            rhoContribution.resetToZero();
            // Compute the term \sum_i ( t_i psi(x+c_i,t) c_i )
            for (plint iPop = 0; iPop < D::q; ++iPop) {
                plint nextX = iX + D::c[iPop][0];
                plint nextY = iY + D::c[iPop][1];
                T psi = psiField.get(nextX-offsetX, nextY-offsetY);
                for (int iD = 0; iD < D::d; ++iD) {
                    // MISSING LINE:
                    // This line is inside the data processor. Here, we access
                    // the neighbors to add a contribution to the interparticle force
                }
            }
            // Computation and storage of the force due to interaction potential
            Cell<T,Descriptor>& cell = lattice.get(iX,iY);
            //T rho = cell.computeDensity(); // Acceleration formulation
            for (int iD = 0; iD < D::d; ++iD) {
                T psi = psiField.get(iX-offsetX, iY-offsetY);
                // MISSING LINE:
                // This line is inside the data processor. Here, we write the
                // computed interparticle force into an external scalar.
            }
        }
    }
}







/* *************** Class GuoCompositeDynamics *************************** */
// Identical to GuoExternalForceBGKdynamics (externalForceDynamics.hh) but here 
// the collision can be freely adjusted through the concept of composite dynamics
template<typename T, template<typename U> class Descriptor>
class GuoCompositeDynamics : public Dynamics<T,Descriptor> {
public:
/* *************** Construction and Destruction ***************************** */

    GuoCompositeDynamics(Dynamics<T,Descriptor>* baseDynamics_, bool forceIsAnAcceleration = false);
    GuoCompositeDynamics<T,Descriptor>(HierarchicUnserializer& unserializer);
    GuoCompositeDynamics(GuoCompositeDynamics<T,Descriptor> const& rhs);
    virtual ~GuoCompositeDynamics();
    GuoCompositeDynamics& operator=(GuoCompositeDynamics<T,Descriptor> const& rhs);
    virtual GuoCompositeDynamics<T,Descriptor>* clone() const;
    /// Return a unique ID for this class.
    virtual int getId() const;
    /// Clone the object with new BaseDynamics
    virtual GuoCompositeDynamics<T,Descriptor>* cloneWithNewBase (
            Dynamics<T,Descriptor>* baseDynamics_ ) const;
    virtual bool isComposite() const;
    /// Serialize the dynamics object.
    virtual void serialize(HierarchicSerializer& serializer) const;
    /// Un-Serialize the dynamics object.
    virtual void unserialize(HierarchicUnserializer& unserializer);

/* *************** Access to base Dynamics ********************************** */

    virtual void replaceBaseDynamics(Dynamics<T,Descriptor>* newBaseDynamics);
    Dynamics<T,Descriptor>& getBaseDynamics();
    Dynamics<T,Descriptor> const& getBaseDynamics() const;

/* *************** Collision, Equilibrium, and Non-equilibrium ************** */

    /// Implementation of the collision step
    virtual void collide(Cell<T,Descriptor>& cell,
                         BlockStatistics& statistics);

    /// Implementation of the collision step, with imposed macroscopic variables
    virtual void collideExternal(Cell<T,Descriptor>& cell, T rhoBar,
                         Array<T,Descriptor<T>::d> const& j, T thetaBar, BlockStatistics& stat);

    /// Compute equilibrium distribution function
    virtual T computeEquilibrium(plint iPop, T rhoBar, Array<T,Descriptor<T>::d> const& j,
                                 T jSqr, T thetaBar=T()) const;

    virtual void computeEquilibria( Array<T,Descriptor<T>::q>& fEq,  T rhoBar, Array<T,Descriptor<T>::d> const& j,
                                    T jSqr, T thetaBar=T() ) const;

    /// Re-compute particle populations from the leading moments
    virtual void regularize(Cell<T,Descriptor>& cell, T rhoBar, Array<T,Descriptor<T>::d> const& j,
                            T jSqr, Array<T,SymmetricTensor<T,Descriptor>::n> const& PiNeq, T thetaBar=T() ) const;

/* *************** Computation of macroscopic variables ********************* */
    /// Say if velocity in this dynamics is computed as "j" (the order-1 moment
    ///   of the populations) or as "j/rho".
    virtual bool velIsJ() const;

    /// Compute the local particle density in lattice units
    virtual T computeDensity(Cell<T,Descriptor> const& cell) const;

    /// Compute the local pressure in lattice units
    virtual T computePressure(Cell<T,Descriptor> const& cell) const;

    /// Compute the local fluid velocity in lattice units
    virtual void computeVelocity( Cell<T,Descriptor> const& cell,
                                  Array<T,Descriptor<T>::d>& u ) const;

    virtual void computeVelocityExternal( Cell<T,Descriptor> const& cell,
                                          T rhoBar, Array<T,Descriptor<T>::d> const& j, 
                                          Array<T,Descriptor<T>::d>& u ) const;

    /// Compute the temperature in lattice units
    virtual T computeTemperature(Cell<T,Descriptor> const& cell) const;

    /// Compute the "off-equilibrium part of Pi"
    virtual void computePiNeq (
        Cell<T,Descriptor> const& cell, Array<T,SymmetricTensor<T,Descriptor>::n>& PiNeq ) const;
    /// Compute the deviatoric stress tensor
    virtual void computeShearStress (
        Cell<T,Descriptor> const& cell, Array<T,SymmetricTensor<T,Descriptor>::n>& stress ) const;

    /// Compute the heat flux in lattice units
    virtual void computeHeatFlux( Cell<T,Descriptor> const& cell,
                                  Array<T,Descriptor<T>::d>& q ) const;

    /// Compute additional user-defined moments
    virtual void computeMoment( Cell<T,Descriptor> const& cell,
                                plint momentId, T* moment ) const;

/* *************** Switch between population and moment representation ****** */

    /// Number of variables required to decompose a population representation into moments.
    virtual plint numDecomposedVariables(plint order) const;

    /// Decompose from population representation into moment representation.
    virtual void decompose(Cell<T,Descriptor> const& cell, std::vector<T>& rawData, plint order) const;

    /// Recompose from moment representation to population representation.
    virtual void recompose(Cell<T,Descriptor>& cell, std::vector<T> const& rawData, plint order) const;

    /// Change the space and time scales of the variables in moment representation.
    virtual void rescale(std::vector<T>& rawData, T xDxInv, T xDt, plint order) const;
    virtual void rescale(int dxScale, int dtScale) {
        Dynamics<T,Descriptor>::rescale(dxScale, dtScale);
    }

/* *************** Additional moments, intended for internal use ************ */

    /// Compute order-0 moment rho-bar
    virtual T computeRhoBar(Cell<T,Descriptor> const& cell) const;

    /// Compute order-0 moment rho-bar and order-1 moment j
    virtual void computeRhoBarJ(Cell<T,Descriptor> const& cell,
                                T& rhoBar, Array<T,Descriptor<T>::d>& j) const;

    /// Compute order-0 moment rho-bar, order-1 moment j, and order-2
    ///   off-equilibrium moment PiNeq.
    virtual void computeRhoBarJPiNeq(Cell<T,Descriptor> const& cell,
                                     T& rhoBar, Array<T,Descriptor<T>::d>& j,
                                     Array<T,SymmetricTensor<T,Descriptor>::n>& PiNeq) const;

    /// Compute e-bar, which is related to the internal energy
    virtual T computeEbar(Cell<T,Descriptor> const& cell) const;

/* *************** Access to Dynamics variables, e.g. omega ***************** */

    /// Get local relaxation parameter of the dynamics
    virtual T getOmega() const;

    /// Set local relaxation parameter of the dynamics
    virtual void setOmega(T omega_);

    /// Get local value of any generic parameter
    virtual T getParameter(plint whichParameter) const;

    /// Set local value of any generic parameter
    virtual void setParameter(plint whichParameter, T value);

/* *************** Access Cell raw data through Dynamics ********************* */

    /// Access particle populations through the dynamics object.
    virtual void getPopulations(Cell<T,Descriptor> const& cell, Array<T,Descriptor<T>::q>& f) const;

    /// Access external fields through the dynamics object.
    virtual void getExternalField (
            Cell<T,Descriptor> const& cell, plint pos, plint size, T* ext ) const;

    /// Define particle populations through the dynamics object.
    virtual void setPopulations(Cell<T,Descriptor>& cell, Array<T,Descriptor<T>::q> const& f);

    /// Define external fields through the dynamics object.
    virtual void setExternalField (
            Cell<T,Descriptor>& cell, plint pos, plint size, const T* ext);

/* *************** Define macroscopic variables, e.g. on boundaries ********* */

    /// Define density, if possible. Does nothing by default.
    virtual void defineDensity(Cell<T,Descriptor>& cell, T density);

    /// Define velocity, if possible. Does nothing by default.
    virtual void defineVelocity(Cell<T,Descriptor>& cell, Array<T,Descriptor<T>::d> const& u);

    /// Define temperature, if possible. Does nothing by default.
    virtual void defineTemperature(Cell<T,Descriptor>& cell, T temperature);

    /// Define heat flux, if possible. Does nothing by default.
    virtual void defineHeatFlux(Cell<T,Descriptor>& cell, Array<T,Descriptor<T>::d> const& q);

    /// Define deviatoric stress, if possible. Does nothing by default.
    virtual void definePiNeq(Cell<T,Descriptor>& cell,
                                        Array<T,SymmetricTensor<T,Descriptor>::n> const& PiNeq);

    /// Define user define moment, if possible. Does nothing by default.
    virtual void defineMoment(Cell<T,Descriptor>& cell, plint momentId, T const* value);
private:
    Dynamics<T,Descriptor>* baseDynamics;
    bool forceIsAnAcceleration;
    static int id;
};



template<typename T, template<typename U> class Descriptor>
int GuoCompositeDynamics<T,Descriptor>::id =
    meta::registerGeneralDynamics<T,Descriptor,GuoCompositeDynamics<T,Descriptor> >("Guo_ExternalForce_Composite");

template<typename T, template<typename U> class Descriptor>
GuoCompositeDynamics<T,Descriptor>::GuoCompositeDynamics(Dynamics<T,Descriptor>* baseDynamics_, bool forceIsAnAcceleration_)
    : baseDynamics(baseDynamics_),
      forceIsAnAcceleration(forceIsAnAcceleration_)
{ }

template<typename T, template<typename U> class Descriptor>
GuoCompositeDynamics<T,Descriptor>::GuoCompositeDynamics(HierarchicUnserializer& unserializer)
    : baseDynamics(0),
      forceIsAnAcceleration(true)
{
    this->unserialize(unserializer);
}

template<typename T, template<typename U> class Descriptor>
GuoCompositeDynamics<T,Descriptor>::GuoCompositeDynamics(GuoCompositeDynamics<T,Descriptor> const& rhs)
    : baseDynamics(rhs.baseDynamics->clone()),
      forceIsAnAcceleration(rhs.forceIsAnAcceleration)
{ }

template<typename T, template<typename U> class Descriptor>
GuoCompositeDynamics<T,Descriptor>::~GuoCompositeDynamics() {
    delete baseDynamics;
}

template<typename T, template<typename U> class Descriptor>
GuoCompositeDynamics<T,Descriptor>& GuoCompositeDynamics<T,Descriptor>::operator=(GuoCompositeDynamics<T,Descriptor> const& rhs)
{
    delete baseDynamics;
    baseDynamics = rhs.baseDynamics->clone();
    forceIsAnAcceleration = rhs.forceIsAnAcceleration;
    return *this;
}

template<typename T, template<typename U> class Descriptor>
GuoCompositeDynamics<T,Descriptor>* GuoCompositeDynamics<T,Descriptor>::clone() const {
    return new GuoCompositeDynamics<T,Descriptor>(*this);
}

template<typename T, template<typename U> class Descriptor>
int GuoCompositeDynamics<T,Descriptor>::getId() const {
    return id;
}

template<typename T, template<typename U> class Descriptor>
GuoCompositeDynamics<T,Descriptor>* GuoCompositeDynamics<T,Descriptor>::cloneWithNewBase (
        Dynamics<T,Descriptor>* baseDynamics_) const
{
    // First, create a clone, based on the dynamic type of GuoCompositeDynamics
    GuoCompositeDynamics<T,Descriptor>* newDynamics = clone();
    // Then, replace its original baseDynamics by the one we've received as parameter
    newDynamics->replaceBaseDynamics(baseDynamics_);
    return newDynamics;
}

template<typename T, template<typename U> class Descriptor>
bool GuoCompositeDynamics<T,Descriptor>::isComposite() const {
    return true;
}

template<typename T, template<typename U> class Descriptor>
bool GuoCompositeDynamics<T,Descriptor>::velIsJ() const {
    return this->getBaseDynamics().velIsJ();
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::replaceBaseDynamics(Dynamics<T,Descriptor>* newBaseDynamics) {
    delete baseDynamics;
    baseDynamics = newBaseDynamics;
}

template<typename T, template<typename U> class Descriptor>
Dynamics<T,Descriptor>& GuoCompositeDynamics<T,Descriptor>::getBaseDynamics() {
    return *baseDynamics;
}

template<typename T, template<typename U> class Descriptor>
Dynamics<T,Descriptor> const& GuoCompositeDynamics<T,Descriptor>::getBaseDynamics() const {
    return *baseDynamics;
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::serialize(HierarchicSerializer& serializer) const
{
    PLB_ASSERT( baseDynamics );
    serializer.addValue(forceIsAnAcceleration);
    serializer.nextDynamics(this->getBaseDynamics().getId());
    this->getBaseDynamics().serialize(serializer);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::unserialize(HierarchicUnserializer& unserializer)
{
    PLB_PRECONDITION( unserializer.getId() == this->getId() );
    forceIsAnAcceleration = unserializer.readValue<bool>();
    // If the composite dynamics is fully constructed, just initialize the variables
    //   from the unserializer.
    if (baseDynamics) {
        this->getBaseDynamics().unserialize(unserializer);
    }
    // If the composite dynamics is being constructed, then the base dynamics must
    //   be newly created.
    else {
        baseDynamics = meta::dynamicsRegistration<T,Descriptor>().generate(unserializer);
    }
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::collide (
        Cell<T,Descriptor>& cell, BlockStatistics& statistics )
{
    Array<T,Descriptor<T>::d> force;
    // MISSING LINE:
    // This line is in the collision term of the dynamics model.
    // We access the external scalar to get the force term previously computed by the data processor.

    Array<T,Descriptor<T>::d> j;
    T rhoBar;
    baseDynamics->computeRhoBarJ(cell, rhoBar, j);
    T rho = Descriptor<T>::fullRho(rhoBar);
    // If the force is an acceleration, e.g., F = g then u[iD] += 0.5 * F[iD] -->  j[iD] += 0.5 * F[iD] * rho
    // otherwise, if it is a force, then  F = rho * g and u[iD] += 0.5 * F[iD] / rho -->  j[iD] += 0.5 * F[iD]
    for (plint iD = 0; iD < Descriptor<T>::d; ++iD)
    {
        j[iD] += 0.5 * force[iD] * (forceIsAnAcceleration? rho: (T)1.);
    }
    T thetaBar = 0.;
    // Guo's forcing term is coded in terms of force, hence one must multiply it by rho
    // in case an acceleration is used (F=g). The amplitude parameter is used to do it.
    baseDynamics -> collideExternal(cell, rhoBar, j, thetaBar, statistics);
    // MISSING LINE
    // This line is in the collision term of the dynamics model.
    // We implement the Guo forcing term to apply the force.
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::collideExternal (
        Cell<T,Descriptor>& cell, T rhoBar,
        Array<T,Descriptor<T>::d> const& j, T thetaBar, BlockStatistics& stat )
{
    baseDynamics -> collideExternal(cell, rhoBar, j, thetaBar, stat);
    T rho = Descriptor<T>::fullRho(rhoBar);
    // Guo's forcing term is coded in terms of force, hence one must multiply it by rho
    // in case an acceleration is used (F=g). The amplitude parameter is used to do it.
    externalForceTemplates<T,Descriptor>::addGuoForce(cell, j, this->getOmega(), forceIsAnAcceleration? rho: (T)1.);
}

template<typename T, template<typename U> class Descriptor>
T GuoCompositeDynamics<T,Descriptor>::computeEquilibrium (
        plint iPop, T rhoBar, Array<T,Descriptor<T>::d> const& j, T jSqr, T thetaBar) const
{
    return baseDynamics -> computeEquilibrium(iPop, rhoBar, j, jSqr, thetaBar);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::computeEquilibria (
         Array<T,Descriptor<T>::q>& fEq, T rhoBar,
         Array<T,Descriptor<T>::d> const& j, T jSqr, T thetaBar ) const
{
    baseDynamics -> computeEquilibria(fEq, rhoBar,j, jSqr, thetaBar);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::regularize (
        Cell<T,Descriptor>& cell, T rhoBar, Array<T,Descriptor<T>::d> const& j,
        T jSqr, Array<T,SymmetricTensor<T,Descriptor>::n> const& PiNeq, T thetaBar ) const
{
    baseDynamics -> regularize(cell, rhoBar, j, jSqr, PiNeq, thetaBar);
}

template<typename T, template<typename U> class Descriptor>
T GuoCompositeDynamics<T,Descriptor>::computeDensity(Cell<T,Descriptor> const& cell) const {
    return this->getBaseDynamics().computeDensity(cell);
}

template<typename T, template<typename U> class Descriptor>
T GuoCompositeDynamics<T,Descriptor>::computePressure(Cell<T,Descriptor> const& cell) const {
    return this->getBaseDynamics().computePressure(cell);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::computeVelocity(Cell<T,Descriptor> const& cell,
                                                          Array<T,Descriptor<T>::d>& u ) const
{
    Array<T,Descriptor<T>::d> j;
    T rhoBar;
    baseDynamics->computeRhoBarJ(cell, rhoBar, j);
    T invRho = Descriptor<T>::invRho(rhoBar);

    Array<T,Descriptor<T>::d> force;
    force.from_cArray(cell.getExternal(Descriptor<T>::ExternalField::forceBeginsAt));
    // If the force is an acceleration, e.g., F = g then u[iD] += 0.5 * F[iD] 
    // otherwise if F = g then u[iD] += 0.5 * F[iD] / rho
    for (plint iD = 0; iD < Descriptor<T>::d; ++iD)
    {
        u[iD] = j[iD] * invRho + 0.5 * force[iD] * (forceIsAnAcceleration? 1.: invRho);
    }
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::computeVelocityExternal (
        Cell<T,Descriptor> const& cell, T rhoBar, Array<T,Descriptor<T>::d> const& j,
        Array<T,Descriptor<T>::d>& u ) const
{
    Array<T,Descriptor<T>::d> force;
    force.from_cArray(cell.getExternal(Descriptor<T>::ExternalField::forceBeginsAt));
    T invRho = Descriptor<T>::invRho(rhoBar);
    // If the force is an acceleration, e.g., F = g then u[iD] += 0.5 * F[iD] 
    // otherwise if F = g then u[iD] += 0.5 * F[iD] / rho
    for (plint iD = 0; iD < Descriptor<T>::d; ++iD)
    {
        u[iD] = j[iD]*invRho + 0.5 * force[iD] * (forceIsAnAcceleration? 1.: invRho);
    }
}

template<typename T, template<typename U> class Descriptor>
T GuoCompositeDynamics<T,Descriptor>::computeTemperature(Cell<T,Descriptor> const& cell) const {
    return this->getBaseDynamics().computeTemperature(cell);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::computePiNeq (
        Cell<T,Descriptor> const& cell, Array<T,SymmetricTensor<T,Descriptor>::n>& PiNeq ) const
{
    return this->getBaseDynamics().computePiNeq(cell, PiNeq);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::computeShearStress (
        Cell<T,Descriptor> const& cell, Array<T,SymmetricTensor<T,Descriptor>::n>& stress ) const
{
    return this->getBaseDynamics().computeShearStress(cell, stress);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::computeHeatFlux(Cell<T,Descriptor> const& cell,
                                                          Array<T,Descriptor<T>::d>& q ) const
{
    this->getBaseDynamics().computeHeatFlux(cell, q);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::computeMoment (
        Cell<T,Descriptor> const& cell, plint momentId, T* moment ) const
{
    this->getBaseDynamics().computeMoment(cell, momentId, moment);
}

template<typename T, template<typename U> class Descriptor>
plint GuoCompositeDynamics<T,Descriptor>::numDecomposedVariables(plint order) const {
    return baseDynamics->numDecomposedVariables(order);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::decompose (
        Cell<T,Descriptor> const& cell, std::vector<T>& rawData, plint order ) const
{
    baseDynamics->decompose(cell, rawData, order);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::recompose (
        Cell<T,Descriptor>& cell, std::vector<T> const& rawData, plint order ) const
{
    baseDynamics->recompose(cell, rawData, order);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::rescale (
        std::vector<T>& rawData, T xDxInv, T xDt, plint order ) const
{
    baseDynamics->rescale(rawData, xDxInv, xDt, order);
}

template<typename T, template<typename U> class Descriptor>
T GuoCompositeDynamics<T,Descriptor>::computeRhoBar(Cell<T,Descriptor> const& cell) const
{
    return this->getBaseDynamics().computeRhoBar(cell);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::computeRhoBarJ (
        Cell<T,Descriptor> const& cell, T& rhoBar, Array<T,Descriptor<T>::d>& j ) const
{
    this->getBaseDynamics().computeRhoBarJ(cell, rhoBar, j);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::computeRhoBarJPiNeq (
        Cell<T,Descriptor> const& cell, T& rhoBar, Array<T,Descriptor<T>::d>& j,
        Array<T,SymmetricTensor<T,Descriptor>::n>& PiNeq ) const
{
    this->getBaseDynamics().computeRhoBarJPiNeq(cell, rhoBar, j, PiNeq);
}

template<typename T, template<typename U> class Descriptor>
T GuoCompositeDynamics<T,Descriptor>::computeEbar(Cell<T,Descriptor> const& cell) const {
    return this->getBaseDynamics().computeEbar(cell);
}

template<typename T, template<typename U> class Descriptor>
T GuoCompositeDynamics<T,Descriptor>::getOmega() const {
    return baseDynamics -> getOmega();
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::setOmega(T omega_) {
    baseDynamics -> setOmega(omega_);
}

template<typename T, template<typename U> class Descriptor>
T GuoCompositeDynamics<T,Descriptor>::getParameter(plint whichParameter) const {
    return baseDynamics -> getParameter(whichParameter);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::setParameter(plint whichParameter, T value) {
    baseDynamics -> setParameter(whichParameter, value);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::getPopulations(Cell<T,Descriptor> const& cell, Array<T,Descriptor<T>::q>& f) const {
    baseDynamics -> getPopulations(cell, f);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::getExternalField(Cell<T,Descriptor> const& cell,
                                                    plint pos, plint size, T* ext) const
{
    baseDynamics -> getExternalField(cell, pos, size, ext);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::setPopulations(Cell<T,Descriptor>& cell, Array<T,Descriptor<T>::q> const& f)
{
    baseDynamics -> setPopulations(cell, f);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::setExternalField(Cell<T,Descriptor>& cell, plint pos,
                                                    plint size, const T* ext)
{
    baseDynamics -> setExternalField(cell, pos, size, ext);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::defineDensity(Cell<T,Descriptor>& cell, T density) {
    baseDynamics -> defineDensity(cell, density);
}


template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::defineVelocity(Cell<T,Descriptor>& cell, Array<T,Descriptor<T>::d> const& u) {
    baseDynamics -> defineVelocity(cell, u);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::defineTemperature(Cell<T,Descriptor>& cell, T temperature) {
    baseDynamics -> defineTemperature(cell, temperature);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::defineHeatFlux(Cell<T,Descriptor>& cell, Array<T,Descriptor<T>::d> const& q) {
    baseDynamics -> defineHeatFlux(cell, q);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::definePiNeq(Cell<T,Descriptor>& cell,
                            Array<T,SymmetricTensor<T,Descriptor>::n> const& PiNeq)
{
    baseDynamics -> definePiNeq(cell, PiNeq);
}

template<typename T, template<typename U> class Descriptor>
void GuoCompositeDynamics<T,Descriptor>::defineMoment(Cell<T,Descriptor>& cell, plint momentId, T const* value) {
    baseDynamics -> defineMoment(cell, momentId, value);
}


}  // namespace plb

#endif  // UTILITY_BUBBLE2D_H
