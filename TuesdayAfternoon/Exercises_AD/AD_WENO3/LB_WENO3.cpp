#include "palabos2D.h"
#include "palabos2D.hh"
#include "headers.h"
#include "headers.hh"
#include <cstdlib>
#include <iostream>

using namespace plb;
using namespace std;

typedef double T;

#define NSDESCRIPTOR descriptors::ForcedD2Q9Descriptor
#define NSDYNAMICS GuoExternalForceBGKdynamics

/// Initialization of the volume fraction field.
template<typename T, template<typename NSU> class nsDescriptor>
struct IniVolFracProcessor2D : public BoxProcessingFunctional2D_S<T>
{
    IniVolFracProcessor2D(plint ny_)
        : ny(ny_)
    { }
    virtual void process(Box2D domain, ScalarField2D<T>& volfracField)
    {
        Dot2D absoluteOffset = volfracField.getLocation();

        T up=0.135;
        T low=0.25;

            for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
                for (plint iY=domain.y0; iY<=domain.y1; ++iY) {

                      plint absoluteY = absoluteOffset.y + iY;

                        	T VolFrac=0.;



    	      if(absoluteY>(ny-1)-util::roundToInt(((up/(up+low))*ny)) && absoluteY<(ny-1)-util::roundToInt((((0.5*up/(up+low)))*ny)))
            {
              VolFrac=1.0;
            }

            volfracField.get(iX, iY) = VolFrac;

            }
        }
    }

    virtual IniVolFracProcessor2D<T,nsDescriptor>* clone() const
    {
        return new IniVolFracProcessor2D<T,nsDescriptor>(*this);
    }

    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
private :
    plint ny;
};


void ExpSetup (
        MultiBlockLattice2D<T, NSDESCRIPTOR>& nsLattice,
        MultiScalarField2D<T>& volfracField)
{

    const plint ny = nsLattice.getNy();
    initializeAtEquilibrium(nsLattice, nsLattice.getBoundingBox(), (T)1., Array<T,2>((T)0.,(T)0.) );

    applyProcessingFunctional(
            new IniVolFracProcessor2D<T,NSDESCRIPTOR>(ny),
            volfracField.getBoundingBox(), volfracField );

    nsLattice.initialize();

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template< typename T, template<typename U> class FluidDescriptor>
  class AdvectionDiffusionFd2D_WENO3 : public BoxProcessingFunctional2D
  {

  public:

      AdvectionDiffusionFd2D_WENO3(Array<T,2> vsed_, T D_, T epsilon_);
      virtual void processGenericBlocks(Box2D domain, std::vector<AtomicBlock2D*> fields );
      virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
      virtual AdvectionDiffusionFd2D_WENO3<T, FluidDescriptor>* clone() const;
    private:
      Array<T,2> vsed;
      T D, epsilon;

  };

template< typename T, template<typename U> class FluidDescriptor>
AdvectionDiffusionFd2D_WENO3<T,FluidDescriptor>::AdvectionDiffusionFd2D_WENO3(Array<T,2> vsed_, T D_, T epsilon_) : vsed(vsed_), D(D_), epsilon(epsilon_)
{ }


template< typename T, template<typename U> class FluidDescriptor>
void AdvectionDiffusionFd2D_WENO3<T,FluidDescriptor>::processGenericBlocks (
        Box2D domain, std::vector<AtomicBlock2D*> fields )
{

    PLB_PRECONDITION(fields.size()==5);
    ScalarField2D<T>* phi_t    = dynamic_cast<ScalarField2D<T>*>(fields[0]);
    ScalarField2D<T>* phi_tp1  = dynamic_cast<ScalarField2D<T>*>(fields[1]);
    ScalarField2D<T>* result   = dynamic_cast<ScalarField2D<T>*>(fields[2]);
    ScalarField2D<T>* Q        = dynamic_cast<ScalarField2D<T>*>(fields[3]);
    BlockLattice2D<T, FluidDescriptor>* fluid = dynamic_cast<BlockLattice2D<T, FluidDescriptor>*>(fields[4]);

    Dot2D ofs1 = computeRelativeDisplacement(*phi_t, *phi_tp1);
    Dot2D ofs2 = computeRelativeDisplacement(*phi_t, *result);
    Dot2D ofs3 = computeRelativeDisplacement(*phi_t, *Q);
    Dot2D ofs4 = computeRelativeDisplacement(*phi_t, *fluid);


    for (plint iX=domain.x0; iX<=domain.x1; ++iX)
    {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY)
        {

           T phiC = phi_tp1->get(iX  +ofs1.x, iY  +ofs1.y);
           T phiE1 = phi_tp1->get(iX+1+ofs1.x, iY  +ofs1.y);
           T phiW1 = phi_tp1->get(iX-1+ofs1.x, iY  +ofs1.y);
           T phiN1 = phi_tp1->get(iX  +ofs1.x, iY+1+ofs1.y);
           T phiS1 = phi_tp1->get(iX  +ofs1.x, iY-1+ofs1.y);

           T phiE2 = phi_tp1->get(iX+2+ofs1.x, iY  +ofs1.y);
           T phiW2 = phi_tp1->get(iX-2+ofs1.x, iY  +ofs1.y);
           T phiN2 = phi_tp1->get(iX  +ofs1.x, iY+2+ofs1.y);
           T phiS2 = phi_tp1->get(iX  +ofs1.x, iY-2+ofs1.y);

           Array<T,2> vel;
           fluid->get(iX+ofs4.x,iY+ofs4.y).computeVelocity(vel);

           Array<T, 2> adv;
           Array<T, 2> fp_p12;
           Array<T, 2> fp_n12;

           Array<T, 2> fp_p12_1;
           Array<T, 2> fp_p12_2;
           Array<T, 2> fp_n12_1;
           Array<T, 2> fp_n12_2;

           Array<T, 2> bp_p12_1;
           Array<T, 2> bp_p12_2;
           Array<T, 2> bp_n12_1;
           Array<T, 2> bp_n12_2;

           Array<T, 2> alpha_p_p12_1;
           Array<T, 2> alpha_p_p12_2;
           Array<T, 2> alpha_p_n12_1;
           Array<T, 2> alpha_p_n12_2;

           Array<T, 2> w1p_p12;
           Array<T, 2> w2p_p12;
           Array<T, 2> w1p_n12;
            Array<T, 2> w2p_n12;
           T diffX, diffY;

           if (util::greaterThan(vel[0], (T) 0)) {
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_p12x
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             fp_p12_1[0] = (phiC + phiE1)/(T)2;
             fp_p12_2[0] = -(phiW1 - (T)3 * phiC)/(T)2;

             bp_p12_1[0] = (phiE1 - phiC)*(phiE1 - phiC);
             bp_p12_2[0] = (phiC - phiW1)*(phiC - phiW1);

           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_n12x
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             fp_n12_1[0] = (phiW1 + phiC)/(T)2;
             fp_n12_2[0] = -(phiW2 - (T)3*phiW1)/(T)2;

             bp_n12_1[0] = (phiC - phiW1)*(phiC - phiW1);
             bp_n12_2[0] = (phiW1 - phiW2)*(phiW1 - phiW2);

           /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


           } else {

             //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             //  fp_p12x
             //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
               fp_p12_1[0] = -(phiE2 - (T)3*phiE1)/(T)2;
               fp_p12_2[0] = (phiE1 + phiC)/(T)2;

               bp_p12_1[0] = (phiE1 - phiE2)*(phiE1 - phiE2);
               bp_p12_2[0] = (phiC - phiE1)*(phiC - phiE1);

           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_n12x
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             fp_n12_1[0] = -(phiE1 - (T)3*phiC)/(T)2;
             fp_n12_2[0] = (phiC + phiW1)/(T)2;

             bp_n12_1[0] = (phiC - phiE1)*(phiC - phiE1);
             bp_n12_2[0] = (phiW1 - phiC)*(phiW1 - phiC);

           /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             // adv[0] = fp_p12x - fp_n12x;


           }

           if (util::greaterThan(vel[1]+vsed[1], (T) 0)) {
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_p12y
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

             fp_p12_1[1] = (phiC + phiN1)/(T)2;
             fp_p12_2[1] = -(phiS1 - (T)3 * phiC)/(T)2;


             bp_p12_1[1] = (phiN1 - phiC)*(phiN1 - phiC);
             bp_p12_2[1] = (phiC - phiS1)*(phiC - phiS1);

           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_n12y
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           fp_n12_1[1] = (phiS1 + phiC)/(T)2;
           fp_n12_2[1] = -(phiS2 - (T)3*phiS1)/(T)2;


           bp_n12_1[1] = (phiC - phiS1)*(phiC - phiS1);
           bp_n12_2[1] = (phiS1 - phiS2)*(phiS1 - phiS2);


           /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             // adv[1] = fp_p12y - fp_n12y;
           } else {


           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_p12y
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           fp_p12_1[1] = -(phiN2 - (T)3*phiN1)/(T)2;
           fp_p12_2[1] = (phiN1 + phiC)/(T)2;

           bp_p12_1[1] = (phiN1 - phiN2)*(phiN1 - phiN2);
           bp_p12_2[1] = (phiC - phiN1)*(phiC - phiN1);

           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           //  fp_n12y
           //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

           fp_n12_1[1] = -(phiN1 - (T)3*phiC)/(T)2;
           fp_n12_2[1] = (phiC + phiS1)/(T)2;

           bp_n12_1[1] = (phiC - phiN1)*(phiC - phiN1);
           bp_n12_2[1] = (phiS1 - phiC)*(phiS1 - phiC);


           }

      for(plint i = 0; i <= 1; i++){

        alpha_p_p12_1[i] = (T)2 / ((T)3 * (epsilon + bp_p12_1[i])*(epsilon + bp_p12_1[i]));
        alpha_p_p12_2[i] = (T)1 / ((T)3 * (epsilon + bp_p12_2[i])*(epsilon + bp_p12_2[i]));
        alpha_p_n12_1[i] = (T)2 / ((T)3 * (epsilon + bp_n12_1[i])*(epsilon + bp_n12_1[i]));
        alpha_p_n12_2[i] = (T)1 / ((T)3 * (epsilon + bp_n12_2[i])*(epsilon + bp_n12_2[i]));


        w1p_p12[i] = alpha_p_p12_1[i] / (alpha_p_p12_1[i] + alpha_p_p12_2[i]);
        w2p_p12[i] = alpha_p_p12_2[i] / (alpha_p_p12_1[i] + alpha_p_p12_2[i]);
        w1p_n12[i] = alpha_p_n12_1[i] / (alpha_p_n12_1[i] + alpha_p_n12_2[i]);
        w2p_n12[i] = alpha_p_n12_2[i] / (alpha_p_n12_1[i] + alpha_p_n12_2[i]);


        fp_p12[i] = w1p_p12[i] * fp_p12_1[i] + w2p_p12[i] * fp_p12_2[i];
        fp_n12[i] = w1p_n12[i] * fp_n12_1[i] + w2p_n12[i] * fp_n12_2[i];

      }

      adv = fp_p12 - fp_n12;



      diffX = phiW1 + phiE1 - (T) 2 * phiC;
      diffY = phiS1 + phiN1 - (T) 2 * phiC;

      T advection = vel[0] * adv[0] + (vel[1]+vsed[1]) * adv[1];
      T diffusion = D * (diffX + diffY);

      result->get(iX+ofs2.x,iY+ofs2.y) = diffusion - advection + Q->get(iX+ofs3.x,iY+ofs3.y);

        }
    }
}

template< typename T, template<typename U> class FluidDescriptor>
AdvectionDiffusionFd2D_WENO3<T,FluidDescriptor>*
    AdvectionDiffusionFd2D_WENO3<T,FluidDescriptor>::clone() const
{
    return new AdvectionDiffusionFd2D_WENO3<T,FluidDescriptor>(*this);
}

template< typename T, template<typename U> class FluidDescriptor>
void AdvectionDiffusionFd2D_WENO3<T,FluidDescriptor>::getTypeOfModification(std::vector<modif::ModifT>& modified) const {
    modified[0] = modif::nothing;
    modified[1] = modif::nothing;
    modified[2] = modif::staticVariables;
    modified[3] = modif::nothing;
    modified[4] = modif::nothing;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void writeVTK(MultiBlockLattice2D<T,NSDESCRIPTOR>& nsLattice,
              MultiScalarField2D<T>& volfracField, plint iter, T dx, T dt)
{


    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iter, 6), dx);
    vtkOut.writeData<float>(volfracField, "VolFrac", (T)1);

}

void writeGif(MultiBlockLattice2D<T,NSDESCRIPTOR>& nsLattice,
              MultiScalarField2D<T>& volfracField, int iT)
{
    const plint imSize = 600;
    const plint nx = nsLattice.getNx();
    const plint ny = nsLattice.getNy();
    Box2D slice(0, nx-1, 0, ny-1);
    ImageWriter<T> imageWriter("leeloo.map");

    imageWriter.writeScaledGif(createFileName("VolFrac", iT, 6),
                               *extractSubDomain(volfracField, slice),
                               imSize, imSize);

    imageWriter.writeScaledGif(createFileName("u", iT, 6),
                               *computeVelocityNorm(nsLattice, slice),
                               imSize, imSize);

}


int main(int argc, char *argv[])
{
    plbInit(&argc, &argv);
    global::timer("simTime").start();



    const T lx  = 0.303;
    const T ly  = 0.385;
    const T uLb = 0.005;
    const T nsTau = 1.0;
    const T D = 0;
    const T epsilon = 1e-6;


    Array<T,2> vsed(0., -uLb);

    const plint resolution = 1000;

    global::directories().setOutputDir("./tmp/");

    T dx = 1./resolution;
    T dt = uLb * dx;

    plint nx =  (lx/dx+(T)0.5) + 1;
    plint ny =  (ly/dx+(T)0.5) + 1;


    T nsOmega = 1.0/nsTau;



    Dynamics<T,NSDESCRIPTOR>* nsdynamics = new NSDYNAMICS<T,NSDESCRIPTOR>(nsOmega);
    MultiBlockLattice2D<T,NSDESCRIPTOR> nsLattice(nx,ny, nsdynamics->clone());
    defineDynamics(nsLattice, nsLattice.getBoundingBox(), nsdynamics->clone());
    delete nsdynamics; nsdynamics = 0;


    SparseBlockStructure2D blockStructure(createRegularDistribution2D(nx, ny));
    plint envelopeWidth = 2;
    MultiScalarField2D<T> volfracField(MultiBlockManagement2D(blockStructure, defaultMultiBlockPolicy2D().getThreadAttribution(), envelopeWidth ),
    defaultMultiBlockPolicy2D().getBlockCommunicator(),
    defaultMultiBlockPolicy2D().getCombinedStatistics(),
    defaultMultiBlockPolicy2D().getMultiScalarAccess<T>(), T());

    MultiScalarField2D<T> phi_t(volfracField), phi_tp1(volfracField), Q(volfracField), phi_n_adv(volfracField), phi_1(volfracField), phi_1_adv(volfracField), phi_2(volfracField), phi_2_adv(volfracField), volfracField_RK(volfracField);


    nsLattice.periodicity().toggleAll(true);
    volfracField.periodicity().toggleAll(true);
    phi_t.periodicity().toggleAll(true);
    phi_tp1.periodicity().toggleAll(true);
    Q.periodicity().toggleAll(true);


    nsLattice.toggleInternalStatistics(false);


    ExpSetup(nsLattice, volfracField);


    T tIni = global::timer("simTime").stop();
    pcout << "time elapsed for ExpSetup:" << tIni << endl;
    global::timer("simTime").start();

    plint evalTime =5000;
    plint iT = 0;
    plint maxT = 0.2/dt;
    // plint maxT = 1;
    plint saveIter = 0.005/dt;
    util::ValueTracer<T> converge((T)1,(T)100,1.0e-3);

    pcout << "Max Number of iterations: " << maxT << endl;
    pcout << "Number of saving iterations: " << saveIter << endl;


    for (iT = 0; iT <= maxT; ++iT)
    {

        if (iT == (evalTime))
        {
            T tEval = global::timer("simTime").stop();
            T remainTime = (tEval - tIni) / (T)evalTime * (T)maxT/(T)3600;
            global::timer("simTime").start();
            pcout << "Remaining " << (plint)remainTime << " hours, and ";
            pcout << (plint)((T)60*(remainTime - (T)((plint)remainTime))+0.5) << " minutes." << endl;
        }
//


        if (iT % saveIter == 0)
        {


            pcout << iT * dt << " : Writing VTK." << endl;
            writeVTK(nsLattice, volfracField, iT, dx, dt);


            pcout << iT * dt << " : Writing gif." << endl;
            writeGif(nsLattice,volfracField, iT);



        }

        nsLattice.collideAndStream();

        plb::copy(volfracField, phi_t, volfracField.getBoundingBox());
        plb::copy(volfracField, phi_tp1, volfracField.getBoundingBox());
        std::vector<MultiBlock2D*> args1;
        args1.push_back(&phi_t);
        args1.push_back(&phi_tp1);
        args1.push_back(&phi_n_adv);
        args1.push_back(&Q);
        args1.push_back(&nsLattice);
        applyProcessingFunctional (new AdvectionDiffusionFd2D_WENO3<T,NSDESCRIPTOR> (vsed, D*(dt/(dx*dx)), epsilon), nsLattice.getBoundingBox(), args1);
        std::vector<MultiBlock2D*> arg_RK1;
        arg_RK1.push_back(&volfracField);
        arg_RK1.push_back(&phi_n_adv);
        arg_RK1.push_back(&phi_1);
        applyProcessingFunctional (new RK3_Step1_functional2D<T> (), nsLattice.getBoundingBox(), arg_RK1);

        plb::copy(phi_1, phi_t, volfracField.getBoundingBox());
        plb::copy(phi_1, phi_tp1, volfracField.getBoundingBox());
        std::vector<MultiBlock2D*> args2;
        args2.push_back(&phi_t);
        args2.push_back(&phi_tp1);
        args2.push_back(&phi_1_adv);
        args2.push_back(&Q);
        args2.push_back(&nsLattice);
        applyProcessingFunctional (new AdvectionDiffusionFd2D_WENO3<T,NSDESCRIPTOR> (vsed, D*(dt/(dx*dx)), epsilon), nsLattice.getBoundingBox(), args2);
        std::vector<MultiBlock2D*> arg_RK2;
        arg_RK2.push_back(&volfracField);
        arg_RK2.push_back(&phi_1_adv);
        arg_RK2.push_back(&phi_2);
        applyProcessingFunctional (new RK3_Step1_functional2D<T> (), nsLattice.getBoundingBox(), arg_RK2);

        plb::copy(phi_2, phi_t, volfracField.getBoundingBox());
        plb::copy(phi_2, phi_tp1, volfracField.getBoundingBox());
        std::vector<MultiBlock2D*> args3;
        args3.push_back(&phi_t);
        args3.push_back(&phi_tp1);
        args3.push_back(&phi_2_adv);
        args3.push_back(&Q);
        args3.push_back(&nsLattice);
        applyProcessingFunctional (new AdvectionDiffusionFd2D_WENO3<T,NSDESCRIPTOR> (vsed, D*(dt/(dx*dx)), epsilon), nsLattice.getBoundingBox(), args3);
        std::vector<MultiBlock2D*> arg_RK3;
        arg_RK3.push_back(&volfracField);
        arg_RK3.push_back(&phi_2_adv);
        arg_RK3.push_back(&volfracField_RK);
        applyProcessingFunctional (new RK3_Step1_functional2D<T> (), nsLattice.getBoundingBox(), arg_RK3);
        plb::copy(volfracField_RK, volfracField, volfracField.getBoundingBox());




    }

    writeGif(nsLattice,volfracField, iT);

    T tEnd = global::timer("simTime").stop();

    T totalTime = tEnd-tIni;
    T nx100 = nsLattice.getNx()/(T)100;
    T ny100 = nsLattice.getNy()/(T)100;
    pcout << "N=" << resolution << endl;
    pcout << "number of processors: " << global::mpi().getSize() << endl;
    pcout << "simulation time: " << totalTime << endl;
    pcout << "total time: " << tEnd << endl;
    pcout << "total iterations: " << iT << endl;
    pcout << "Msus: " << nx100*ny100*ny100*(T)iT/totalTime << endl;

    return 0;
}
