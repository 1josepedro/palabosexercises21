#ifndef RK3_H
#define RK3_H

#include "core/globalDefs.h"
#include "core/block2D.h"
#include "atomicBlock/dataProcessor2D.h"
#include "atomicBlock/blockLattice2D.h"

namespace plb {

/* ******** RK3_Step1_functional2D ****************************************** */

template<typename T>
class RK3_Step1_functional2D : public ScalarFieldBoxProcessingFunctional2D<T>
{
public:
    virtual void process(Box2D domain, std::vector<ScalarField2D<T>*> scalarFields);
    virtual RK3_Step1_functional2D<T>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
};

  /* ******** RK3_Step2_functional2D ****************************************** */

template<typename T>
class RK3_Step2_functional2D : public ScalarFieldBoxProcessingFunctional2D<T>
{
public:
    virtual void process(Box2D domain, std::vector<ScalarField2D<T>*> scalarFields);
    virtual RK3_Step2_functional2D<T>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
};

/* ******** RK3_Step3_functional2D ****************************************** */

template<typename T>
class RK3_Step3_functional2D : public ScalarFieldBoxProcessingFunctional2D<T>
{
public:
  virtual void process(Box2D domain, std::vector<ScalarField2D<T>*> scalarFields);
  virtual RK3_Step3_functional2D<T>* clone() const;
  virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
  virtual BlockDomain::DomainT appliesTo() const;
};

}

#endif
