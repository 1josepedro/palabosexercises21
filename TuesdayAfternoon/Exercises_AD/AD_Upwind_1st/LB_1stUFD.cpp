#include "palabos2D.h"
#include "palabos2D.hh"

#include <cstdlib>
#include <iostream>

using namespace plb;
using namespace std;

typedef double T;

#define NSDESCRIPTOR descriptors::ForcedD2Q9Descriptor
#define NSDYNAMICS GuoExternalForceBGKdynamics

/// Initialization of the volume fraction field.
template<typename T, template<typename NSU> class nsDescriptor>
struct IniVolFracProcessor2D : public BoxProcessingFunctional2D_S<T>
{
    IniVolFracProcessor2D(plint ny_)
        : ny(ny_)
    { }
    virtual void process(Box2D domain, ScalarField2D<T>& volfracField)
    {
        Dot2D absoluteOffset = volfracField.getLocation();
        T up=0.135;
        T low=0.25;
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                 plint absoluteY = absoluteOffset.y + iY;
                    T VolFrac=0.;
                    if(absoluteY>(ny-1)-util::roundToInt(((up/(up+low))*ny)) && absoluteY<(ny-1)-util::roundToInt((((0.5*up/(up+low)))*ny))) VolFrac=1.0;
                    volfracField.get(iX, iY) = VolFrac;
            }
        }
    }

    virtual IniVolFracProcessor2D<T,nsDescriptor>* clone() const
    {
        return new IniVolFracProcessor2D<T,nsDescriptor>(*this);
    }

    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
private :
    plint ny;
};

void ExpSetup (
        MultiBlockLattice2D<T, NSDESCRIPTOR>& nsLattice,
        MultiScalarField2D<T>& volfracField)
{
    const plint ny = nsLattice.getNy();
    initializeAtEquilibrium(nsLattice, nsLattice.getBoundingBox(), (T)1., Array<T,2>((T)0.,(T)0.) );
    applyProcessingFunctional(new IniVolFracProcessor2D<T,NSDESCRIPTOR>(ny), volfracField.getBoundingBox(), volfracField );
    nsLattice.initialize();
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Data processor for the 1st order upwind finite difference scheme
template< typename T, template<typename U> class FluidDescriptor>
  class AdvectionDiffusionFd2D : public BoxProcessingFunctional2D
  {
  public:
      AdvectionDiffusionFd2D(Array<T,2> vsed_, T D_);
      virtual void processGenericBlocks(Box2D domain, std::vector<AtomicBlock2D*> fields );
      virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
      virtual AdvectionDiffusionFd2D<T, FluidDescriptor>* clone() const;
    private:
      Array<T,2> vsed;
      T D;
  };

template< typename T, template<typename U> class FluidDescriptor>
AdvectionDiffusionFd2D<T,FluidDescriptor>::AdvectionDiffusionFd2D(Array<T,2> vsed_, T D_) : vsed(vsed_), D(D_)
{ }


template< typename T, template<typename U> class FluidDescriptor>
void AdvectionDiffusionFd2D<T,FluidDescriptor>::processGenericBlocks (
        Box2D domain, std::vector<AtomicBlock2D*> fields )
{
    PLB_PRECONDITION(fields.size()==5);
    ScalarField2D<T>* phi_t    = dynamic_cast<ScalarField2D<T>*>(fields[0]);
    ScalarField2D<T>* phi_tp1  = dynamic_cast<ScalarField2D<T>*>(fields[1]);
    ScalarField2D<T>* result   = dynamic_cast<ScalarField2D<T>*>(fields[2]);
    ScalarField2D<T>* Q        = dynamic_cast<ScalarField2D<T>*>(fields[3]);
    BlockLattice2D<T, FluidDescriptor>* fluid = dynamic_cast<BlockLattice2D<T, FluidDescriptor>*>(fields[4]);

    Dot2D ofs1 = computeRelativeDisplacement(*phi_t, *phi_tp1);
    Dot2D ofs2 = computeRelativeDisplacement(*phi_t, *result);
    Dot2D ofs3 = computeRelativeDisplacement(*phi_t, *Q);
    Dot2D ofs4 = computeRelativeDisplacement(*phi_t, *fluid);

    for (plint iX=domain.x0; iX<=domain.x1; ++iX)
    {
        for (plint iY=domain.y0; iY<=domain.y1; ++iY)
        {

           T phiC = phi_tp1->get(iX  +ofs1.x, iY  +ofs1.y);
           T phiE = phi_tp1->get(iX+1+ofs1.x, iY  +ofs1.y);
           T phiW = phi_tp1->get(iX-1+ofs1.x, iY  +ofs1.y);
           T phiN = phi_tp1->get(iX  +ofs1.x, iY+1+ofs1.y);
           T phiS = phi_tp1->get(iX  +ofs1.x, iY-1+ofs1.y);

           Array<T,2> vel;
           fluid->get(iX+ofs4.x,iY+ofs4.y).computeVelocity(vel);

           Array<T, 2> adv;
           T diffX, diffY;

           adv[0] = (util::greaterThan(vel[0], (T) 0) ? (phiC - phiW) : (util::lessThan(   vel[0], (T) 0) ? (phiE - phiC) : (T) 0.5 * (phiE - phiW)));
           adv[1] = (util::greaterThan(vel[1]+vsed[1], (T) 0) ? (phiC - phiS) : (util::lessThan(vel[1]+vsed[1], (T) 0) ? (phiN - phiC) : (T) 0.5 * (phiN - phiS)));

           diffX = phiW + phiE - (T) 2 * phiC;
           diffY = phiS + phiN - (T) 2 * phiC;

           T advection = vel[0] * adv[0] + (vel[1]+vsed[1]) * adv[1];
           T diffusion = D * (diffX + diffY);

           result->get(iX+ofs2.x,iY+ofs2.y) = phi_t->get(iX,iY) + diffusion - advection + Q->get(iX+ofs3.x,iY+ofs3.y);

        }
    }
}

template< typename T, template<typename U> class FluidDescriptor>
AdvectionDiffusionFd2D<T,FluidDescriptor>*
    AdvectionDiffusionFd2D<T,FluidDescriptor>::clone() const
{
    return new AdvectionDiffusionFd2D<T,FluidDescriptor>(*this);
}

template< typename T, template<typename U> class FluidDescriptor>
void AdvectionDiffusionFd2D<T,FluidDescriptor>::getTypeOfModification(std::vector<modif::ModifT>& modified) const {
    modified[0] = modif::nothing;
    modified[1] = modif::nothing;
    modified[2] = modif::staticVariables;
    modified[3] = modif::nothing;
    modified[4] = modif::nothing;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void writeVTK(MultiBlockLattice2D<T,NSDESCRIPTOR>& nsLattice,
              MultiScalarField2D<T>& volfracField, plint iter, T dx, T dt)
{
    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iter, 6), dx);
    vtkOut.writeData<float>(volfracField, "VolFrac", (T)1);
}

void writeGif(MultiBlockLattice2D<T,NSDESCRIPTOR>& nsLattice,
              MultiScalarField2D<T>& volfracField, int iT)
{
    const plint imSize = 600;
    const plint nx = nsLattice.getNx();
    const plint ny = nsLattice.getNy();
    Box2D slice(0, nx-1, 0, ny-1);
    ImageWriter<T> imageWriter("leeloo.map");

    imageWriter.writeScaledGif(createFileName("VolFrac", iT, 6),
                               *extractSubDomain(volfracField, slice),
                               imSize, imSize);
}

int main(int argc, char *argv[])
{
    plbInit(&argc, &argv);
    global::timer("simTime").start();

    const T lx  = 0.303;
    const T ly  = 0.385;
    const T uLb = 0.005;
    const T nsTau = 1.0;
    const T D = 2e-9;

    Array<T,2> vsed(0., -uLb);

    const plint resolution = 1000;

    global::directories().setOutputDir("./tmp/");

    T dx = 1./resolution;
    T dt = uLb * dx;

    plint nx =  (lx/dx+(T)0.5) + 1;
    plint ny =  (ly/dx+(T)0.5) + 1;

    T nsOmega = 1.0/nsTau;

    Dynamics<T,NSDESCRIPTOR>* nsdynamics = new NSDYNAMICS<T,NSDESCRIPTOR>(nsOmega);
    MultiBlockLattice2D<T,NSDESCRIPTOR> nsLattice(nx,ny, nsdynamics->clone());
    defineDynamics(nsLattice, nsLattice.getBoundingBox(), nsdynamics->clone());
    delete nsdynamics; nsdynamics = 0;

    MultiScalarField2D<T> volfracField(nx, ny);
    MultiScalarField2D<T> phi_t(volfracField), phi_tp1(volfracField), Q(volfracField);

    nsLattice.periodicity().toggleAll(true);
    volfracField.periodicity().toggleAll(true);
    phi_t.periodicity().toggleAll(true);
    phi_tp1.periodicity().toggleAll(true);
    Q.periodicity().toggleAll(true);

    nsLattice.toggleInternalStatistics(false);

    ExpSetup(nsLattice, volfracField);

    T tIni = global::timer("simTime").stop();
    pcout << "time elapsed for ExpSetup:" << tIni << endl;
    global::timer("simTime").start();

    plint evalTime =5000;
    plint iT = 0;
    plint maxT = 0.2/dt;
    // plint maxT = 1;
    plint saveIter = 0.005/dt;

    pcout << "Max Number of iterations: " << maxT << endl;
    pcout << "Number of saving iterations: " << saveIter << endl;

    for (iT = 0; iT <= maxT; ++iT)
    {

        if (iT == (evalTime))
        {
            T tEval = global::timer("simTime").stop();
            T remainTime = (tEval - tIni) / (T)evalTime * (T)maxT/(T)3600;
            global::timer("simTime").start();
            pcout << "Remaining " << (plint)remainTime << " hours, and ";
            pcout << (plint)((T)60*(remainTime - (T)((plint)remainTime))+0.5) << " minutes." << endl;
        }

        if (iT % saveIter == 0)
        {

            pcout << iT * dt << " : Writing VTK." << endl;
            writeVTK(nsLattice, volfracField, iT, dx, dt);

            pcout << iT * dt << " : Writing gif." << endl;
            writeGif(nsLattice,volfracField, iT);

        }

        nsLattice.collideAndStream();

        plb::copy(volfracField, phi_t, volfracField.getBoundingBox());
        plb::copy(volfracField, phi_tp1, volfracField.getBoundingBox());
        std::vector<MultiBlock2D*> args;
        args.push_back(&phi_t);
        args.push_back(&phi_tp1);
        args.push_back(&volfracField);
        args.push_back(&Q);
        args.push_back(&nsLattice);

        applyProcessingFunctional (new AdvectionDiffusionFd2D<T,NSDESCRIPTOR> (vsed, D*(dt/(dx*dx))), nsLattice.getBoundingBox(), args);

    }

    writeGif(nsLattice,volfracField, iT);

    T tEnd = global::timer("simTime").stop();

    T totalTime = tEnd-tIni;
    T nx100 = nsLattice.getNx()/(T)100;
    T ny100 = nsLattice.getNy()/(T)100;
    pcout << "N=" << resolution << endl;
    pcout << "number of processors: " << global::mpi().getSize() << endl;
    pcout << "simulation time: " << totalTime << endl;
    pcout << "total time: " << tEnd << endl;
    pcout << "total iterations: " << iT << endl;
    pcout << "Msus: " << nx100*ny100*ny100*(T)iT/totalTime << endl;

    return 0;
}
