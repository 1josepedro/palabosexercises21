# Advection-Diffusion with sharp interfaces

## Guide
#### Install cmake and paraview
To compile this example you need Cmake installed in your system. To check the installation
type on your terminal: `cmake --version`.
If it returns an error, you need to proceed with the installation. Try with the following command:
* archlinux based (manjaro): `sudo pacman -S cmake`
* debian based (ubuntu): `sudo apt install cmake`
* redhat based (opensuse,fedora, centos): `yum install cmake`

for other systems, please refer to the [official installation instructions](https://cmake.org/install/).

You will also need to have installed `paraview` in your machine to read the output files. Test if it is already installed with:
`paraview --version`. If this is not the case, try to install them
using your system specific command:
* archlinux based: `sudo pacman -S gifsicle` and `sudo pacman -S paraview`
* debian based (ubuntu):
* redhat based (opensuse,fedora, centos):

#### Test the compilation
If Palabos is on the directory of the exercises, the provided CMakeLists.txt
should already be operative. In all the other cases, you can check the following lines  
of the CMakeLists.txt to modify the palabos location:
```
# # # ADD PALABOS PATH:
# # CASE 1: PALABOS_ROOT is on your path
# file(TO_CMAKE_PATH $ENV{PALABOS_ROOT} PALABOS_ROOT)
# # CASE 2: specify an ABSOLUTE DIRECTORY PATH
#set(PALABOS_ROOT path/to/palabos)
# # CASE 3: specify a relative path to the build folder
set(PALABOS_ROOT ${CMAKE_CURRENT_SOURCE_DIR}/../palabos)
```

Open a terminal in the current directory and type the following commands
to compile the code with cmake:
```
cd build
cmake .. && make -j 2
cd ..
```
At this point you should have an executable in the current directory.
Try to run it in parallel
```
mpirun -np 2 yourexecutable
```
in the case of a linux-based system.


## Introduction

The exercises aim at simulating a quantity transported by a fluid and thus, how to implement the coupling with different numerical strategies.
We will focus here on problems involving sharp interfaces and low diffusivity.  

###Exercise 1

The goal of this exercise is to familiarize with different numerical schemes that solve the Advection-Diffusion equation (ADE).
The main idea is to highlight the pros and cons of each scheme in a particular configuration involving sharp interfaces.
In the case of a quantity described by an advection-diffusion law and transported by a fluid, it is also useful to identify how we can couple the presented schemes with the fluid velocity.

Eventually, the exercise will allow to evaluate the different schemes for advection-dominated problems i.e. with low diffusivity.
For now, no forcing term are implemented as we only want to focus on the velocity coupling and the problem of low diffusivity.

1. A full Lattice Boltzmann (LB) program is implemented i.e. LB for the fluid AND for the advection-diffusion equation.
2. A hybrid LB + 1st-order Upwind Finite difference scheme is implemented.
3. The same code is given but with a second-order implementation.
4. Finally a more complex scheme involving the 3rd order WENO Finite difference scheme is presented.

Go through the codes and identify how the fluid velocity is injected in the convective term of the ADE. Pay also attention on the code structure and field initialization (envelope size...).
Run the program and play with some parameters, especially the diffusion coefficient and observe what happens (stability, numerical diffusion...).

Open the outputs with paraview, plot a vertical profile of volume fraction and see hoy it evolves through time, for each case.
Summarize everything in order to extract some pros and cons.

###Exercise 2

This exercise is an application to what we can call a settling-driven Rayleigh-Taylor instability. When the transported quantity crosses the initial interface, a dense layer forms, starts to deform and collapse as vertical fingers. It is interesting now to study how we can couple the ADE with the fluid through a forcing term.
Indeed, how the presence of any transported quantity affects the fluid motion (we will consider here the Boussinesq approximation).
It involves three fields (three-way coupling).
  - A density field which describes a situation where the fluid density has a discontinuity in the y direction (for instance a two-layers system with an upper layer of fresh water is placed above a denser lower layer of water+sugar). In the example, the density field is solved using the 1st order Upwind scheme as the diffusity is not critical here.
  - A field describing the carrier fluid and solved using the LBM.
  - A volume fraction field describing a quantity which settles within the fluid (with no drag). This field is solved usinf the 3rd order WENO scheme.

Here there is a buoyant force term (given during the presentation) to be implemented within a data processor. Try to implement this force term and run the simulation with a "coarse" resolution.
Play with the settling velocity, the initial particle volume fraction...Observe the downward fingers developing and if there is time, increase the mesh resolution to observe the better details.
