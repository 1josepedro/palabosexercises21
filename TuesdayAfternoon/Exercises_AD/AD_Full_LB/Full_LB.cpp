#include "palabos2D.h"
#include "palabos2D.hh"

#include <cstdlib>
#include <iostream>

using namespace plb;
using namespace std;

typedef double T;

#define NSDESCRIPTOR descriptors::ForcedD2Q9Descriptor
#define ADESCRIPTOR descriptors::AdvectionDiffusionD2Q5Descriptor

#define ADYNAMICS AdvectionDiffusionBGKdynamics
#define NSDYNAMICS GuoExternalForceBGKdynamics

/// Initialization of the volume fraction field.
template<typename T, template<typename NSU> class nsDescriptor, template<typename ADU> class adDescriptor>
struct IniVolFracProcessor2D : public BoxProcessingFunctional2D_L<T,adDescriptor>
{
    IniVolFracProcessor2D(plint ny_)
        : ny(ny_)
    { }
    virtual void process(Box2D domain, BlockLattice2D<T,adDescriptor>& volfracField)
    {
        Dot2D absoluteOffset = volfracField.getLocation();

        T up=0.135;
        T low=0.25;

        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                  plint absoluteY = absoluteOffset.y + iY;
                  T VolFrac=0.;
    	            if(absoluteY>(ny-1)-util::roundToInt(((up/(up+low))*ny)) && absoluteY<(ny-1)-util::roundToInt((((0.5*up/(up+low)))*ny))) VolFrac=1.0;
                  Array<T,adDescriptor<T>::d> jEq(0., 0.);
                  volfracField.get(iX,iY).defineDensity(VolFrac);
                  iniCellAtEquilibrium(volfracField.get(iX,iY), VolFrac, jEq);
            }
          }
    }

    virtual IniVolFracProcessor2D<T,nsDescriptor,adDescriptor>* clone() const
    {
        return new IniVolFracProcessor2D<T,nsDescriptor,adDescriptor>(*this);
    }

    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
private :
    plint ny;
};


void ExpSetup (
        MultiBlockLattice2D<T, NSDESCRIPTOR>& nsLattice,
        MultiBlockLattice2D<T, ADESCRIPTOR>& volfracField)
{
    const plint ny = nsLattice.getNy();
    initializeAtEquilibrium(nsLattice, nsLattice.getBoundingBox(), (T)1., Array<T,2>((T)0.,(T)0.) );
    initializeAtEquilibrium(volfracField, volfracField.getBoundingBox(), (T)1., Array<T,2>((T)0.,(T)0.) );
    applyProcessingFunctional(new IniVolFracProcessor2D<T,NSDESCRIPTOR,ADESCRIPTOR>(ny), volfracField.getBoundingBox(), volfracField );
    volfracField.initialize();
    nsLattice.initialize();
}

//Data processor for the velocity coupling
template< typename T, template<typename U1> class FluidDescriptor, template<typename U2> class VolfracDescriptor>
  class VelocityCouplingProcessor2D : public BoxProcessingFunctional2D_LL<T,FluidDescriptor,T,VolfracDescriptor>
  {
  public:
      VelocityCouplingProcessor2D(Array<T,2> vsed_);
      virtual void process( Box2D domain,BlockLattice2D<T,FluidDescriptor>& fluid, BlockLattice2D<T,VolfracDescriptor>& VolFrac );
      virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
      virtual VelocityCouplingProcessor2D<T,FluidDescriptor,VolfracDescriptor>* clone() const;
    private:
      Array<T,2> vsed;
  };

template< typename T, template<typename U1> class FluidDescriptor, template<typename U2> class VolfracDescriptor>
VelocityCouplingProcessor2D<T,FluidDescriptor,VolfracDescriptor>::VelocityCouplingProcessor2D(Array<T,2> vsed_) : vsed(vsed_)
{ }

template< typename T, template<typename U1> class FluidDescriptor, template<typename U2> class VolfracDescriptor>
void VelocityCouplingProcessor2D<T,FluidDescriptor,VolfracDescriptor>::process (
        Box2D domain,
        BlockLattice2D<T,FluidDescriptor>& fluid,
        BlockLattice2D<T,VolfracDescriptor>& VolFrac )
{
    enum {velOffset   = VolfracDescriptor<T>::ExternalField::velocityBeginsAt,};
    Dot2D offset = computeRelativeDisplacement(fluid, VolFrac);
    for (plint iX=domain.x0; iX<=domain.x1; ++iX){
        for (plint iY=domain.y0; iY<=domain.y1; ++iY){
                T *u = VolFrac.get(iX+offset.x,iY+offset.y).getExternal(velOffset);
                Array<T,FluidDescriptor<T>::d> vel, vel_tot;
                fluid.get(iX,iY).computeVelocity(vel);
                vel_tot = vel + vsed;
                vel_tot.to_cArray(u);
        }
    }
}

template< typename T, template<typename U1> class FluidDescriptor, template<typename U2> class VolfracDescriptor>
VelocityCouplingProcessor2D<T,FluidDescriptor,VolfracDescriptor>*
    VelocityCouplingProcessor2D<T,FluidDescriptor,VolfracDescriptor>::clone() const
{
    return new VelocityCouplingProcessor2D<T,FluidDescriptor,VolfracDescriptor>(*this);
}

template< typename T, template<typename U1> class FluidDescriptor, template<typename U2> class VolfracDescriptor>
void VelocityCouplingProcessor2D<T,FluidDescriptor,VolfracDescriptor>::getTypeOfModification(std::vector<modif::ModifT>& modified) const {
    modified[0] = modif::nothing;
    modified[1] = modif::staticVariables;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void writeVTK(MultiBlockLattice2D<T,NSDESCRIPTOR>& nsLattice,
              MultiBlockLattice2D<T,ADESCRIPTOR>& volfracField, plint iter, T dx, T dt)
{
    VtkImageOutput2D<T> vtkOut(createFileName("vtk", iter, 6), dx);
    vtkOut.writeData<float>(*computeDensity(volfracField), "VolFrac", (T)1);
}

void writeGif(MultiBlockLattice2D<T,NSDESCRIPTOR>& nsLattice,
              MultiBlockLattice2D<T,ADESCRIPTOR>& volfracField, int iT)
{
    const plint imSize = 600;
    const plint nx = nsLattice.getNx();
    const plint ny = nsLattice.getNy();
    Box2D slice(0, nx-1, 0, ny-1);
    ImageWriter<T> imageWriter("leeloo.map");
    imageWriter.writeScaledGif(createFileName("VolFrac", iT, 6),
                               *computeDensity(volfracField, slice),
                               imSize, imSize);
}


int main(int argc, char *argv[])
{
    plbInit(&argc, &argv);
    global::timer("simTime").start();



    const T lx  = 0.303;
    const T ly  = 0.385;
    const T uLb = 0.005;
    const T nsTau = 1.0;
    const T D = 2e-9;


    Array<T,2> vsed(0., -uLb);

    const plint resolution = 1000;

    global::directories().setOutputDir("./tmp/");

    T dx = 1./resolution;
    T dt = uLb * dx;

    T adTau = ADESCRIPTOR<T>::invCs2*(dt*dt/(dx*dx))*D + (dt/2);
    plint nx =  (lx/dx+(T)0.5) + 1;
    plint ny =  (ly/dx+(T)0.5) + 1;


    T nsOmega = 1.0/nsTau;
    T adOmega = dt/adTau;

    pcout << "dx = " << dx << endl;
    pcout << "dt = " << dt << endl;
    pcout << "nx = " << nx << endl;
    pcout << "ny = " << ny << endl;
    pcout << "adOmega = " << adOmega << endl;


    Dynamics<T,NSDESCRIPTOR>* nsdynamics = new NSDYNAMICS<T,NSDESCRIPTOR>(nsOmega);
    MultiBlockLattice2D<T,NSDESCRIPTOR> nsLattice(nx,ny, nsdynamics->clone());
    defineDynamics(nsLattice, nsLattice.getBoundingBox(), nsdynamics->clone());
    delete nsdynamics; nsdynamics = 0;

    Dynamics<T,ADESCRIPTOR>* adynamics = new ADYNAMICS<T,ADESCRIPTOR>(adOmega);
    MultiBlockLattice2D<T,ADESCRIPTOR> volfracField(nx,ny, adynamics->clone());
    defineDynamics(volfracField, volfracField.getBoundingBox(), adynamics->clone());
    delete adynamics; adynamics = 0;

    nsLattice.periodicity().toggleAll(true);
    volfracField.periodicity().toggleAll(true);

    nsLattice.toggleInternalStatistics(false);
    volfracField.toggleInternalStatistics(false);

    ExpSetup(nsLattice, volfracField);

    integrateProcessingFunctional (
              new VelocityCouplingProcessor2D<T,NSDESCRIPTOR,ADESCRIPTOR> (vsed),
                  nsLattice.getBoundingBox(),
                  nsLattice, volfracField, 0);

    T tIni = global::timer("simTime").stop();
    pcout << "time elapsed for ExpSetup:" << tIni << endl;
    global::timer("simTime").start();

    plint evalTime =5000;
    plint iT = 0;
    plint maxT = 0.2/dt;
    plint saveIter = 0.005/dt;

    pcout << "Max Number of iterations: " << maxT << endl;
    pcout << "Number of saving iterations: " << saveIter << endl;

    for (iT = 0; iT <= maxT; ++iT)
    {

        if (iT == (evalTime))
        {
            T tEval = global::timer("simTime").stop();
            T remainTime = (tEval - tIni) / (T)evalTime * (T)maxT/(T)3600;
            global::timer("simTime").start();
            pcout << "Remaining " << (plint)remainTime << " hours, and ";
            pcout << (plint)((T)60*(remainTime - (T)((plint)remainTime))+0.5) << " minutes." << endl;
        }

        if (iT % saveIter == 0)
        {
            pcout << iT * dt << " : Writing VTK." << endl;
            writeVTK(nsLattice, volfracField, iT, dx, dt);

            pcout << iT * dt << " : Writing gif." << endl;
            writeGif(nsLattice,volfracField, iT);
        }

        // Lattice Boltzmann iteration step.
        volfracField.collideAndStream();
        nsLattice.collideAndStream();
    }

    writeGif(nsLattice,volfracField, iT);

    T tEnd = global::timer("simTime").stop();

    T totalTime = tEnd-tIni;
    T nx100 = nsLattice.getNx()/(T)100;
    T ny100 = nsLattice.getNy()/(T)100;
    pcout << "N=" << resolution << endl;
    pcout << "number of processors: " << global::mpi().getSize() << endl;
    pcout << "simulation time: " << totalTime << endl;
    pcout << "total time: " << tEnd << endl;
    pcout << "total iterations: " << iT << endl;
    pcout << "Msus: " << nx100*ny100*ny100*(T)iT/totalTime << endl;

    return 0;
}
