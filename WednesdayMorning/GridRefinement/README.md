# Three exercises

* There are three quite different exercises.
* Each exercise is in it's own directory.
* Please refer to the `README.md` file of each directory for more details.

1. The `generateGridDensityFromBoxes` which is used to get familiar with grid density generation with boxes.
2. The `generateGridDensityFromSpheres` which helps you get familiar with the creation of novel geometries from grid densities.
3. The `offLatticeExternalFlow` which is an exterior flow past a sphere which contain:
   * Grid generation and familiarization.
   * There will then be several data analysis addition to make: add the computation of the Reynolds stress.


